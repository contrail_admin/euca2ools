# Software License Agreement (BSD License)
#
# Copyright (c) 2009-2011, Eucalyptus Systems, Inc.
# All rights reserved.
#
# Redistribution and use of this software in source and binary forms, with or
# without modification, are permitted provided that the following conditions
# are met:
#
#   Redistributions of source code must retain the above
#   copyright notice, this list of conditions and the
#   following disclaimer.
#
#   Redistributions in binary form must reproduce the above
#   copyright notice, this list of conditions and the
#   following disclaimer in the documentation and/or other
#   materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Neil Soman neil@eucalyptus.com
#         Mitch Garnaat mgarnaat@eucalyptus.com

import euca2ools.commands.eucacommand
from boto.roboto.param import Param

class DescribeAddresses(euca2ools.commands.eucacommand.EucaCommand):

    APIVersion = '2013-07-15'
    Description = 'Shows information about addresses.'
    Options = [Param(name='ip', short_name='ip', ptype='string',
                  cardinality='+', optional=True),
               Param(name='allocation_id', short_name='id', ptype='string',
                  cardinality='+', optional=True)]
    Filters = [Param(name='instance-id', ptype='string',
                     doc='Instance the address is associated with (if any).'),
               Param(name='public-ip', ptype='string',
                     doc='The elastic IP address.'),
               Param(name='domain', ptype='string',
                     doc='Set to vpc.')]

    def display_addresses(self, addresses):
        print '%-16s%-10s%-26s%-46s' % ('Address', 'Domain', 'AllocationId', 'InstanceId(AssociationId)')
        print '%-16s%-10s%-26s%-46s' % ('-------', '------', '------------', '-------------------------')
        for address in addresses:
            domain = getattr(address, 'domain', 'standard') or 'standard'
            instance_str = ''
            if address.instance_id:
                instance_str = '%s(%s)' % (address.instance_id, address.association_id)
            print '%-16s%-10s%-26s%-46s' % (address.public_ip,
                                           domain, address.allocation_id or '',
                                           instance_str)

    def main(self):
        conn = self.make_connection_cli('vpc')
        return self.make_request_cli(conn, 'get_all_addresses',
                                     addresses=self.ip, allocation_ids=self.allocation_id,
                                     filters=self.filters)

    def main_cli(self):
        addresses = self.main()
        self.display_addresses(addresses)
